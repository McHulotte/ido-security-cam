import math
import numpy as np
import cv2 as cv
import copy
import functions as f
#import paho.mqtt.client as mqtt

rtsp1="rtsp://camera01.rhex.lirmm.fr:554/media/1/1"
rtsp2="rtsp://camera02.rhex.lirmm.fr:554/media/1/1"

camL = cv.VideoCapture(rtsp1)
camR = cv.VideoCapture(rtsp2)

# If capture not initialized
if not camL.isOpened() or not camR.isOpened() :
    print("Can't read camera")
    exit()

while(True):
    # Capture frame-by-frame
    retValL, frameL = camL.read()
    retValR, frameR = camR.read()
    # If no frame received
    if not retValR or not retValL :
        print("No frame received on camera 1 or 2...\n")
        break
    
    grayL = cv.cvtColor(frameL, cv.COLOR_BGR2GRAY)
    grayL = np.float32(grayL)
    dstL = cv.cornerHarris(grayL,2,3,0.05)

    # Thresholding value to keep only interesting ones
    frameL[dstL>0.01*dstL.max()]=[0, 0, 255]
    # Find pixels with bgr value of 0,255,255
    coordL = np.where(np.all(frameL == (0,0,255), axis=-1))
    # invert axis Y, X and combine 2 Tuple of coordinate into list of coordinates.
    coordinatesL = list(zip(coordL[1],coordL[0]))

    #for i in coordinatesL:
     #   x,y = f.correspondance(frameL,frameR,5,10,i[0],i[1])
     #   frameR[y,x] = [255,0,0]

    # Display the resulting frame
    cv.imshow('frameR',frameR)
    cv.imshow('frameL',frameL)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
camL.release()
camR.release()
cv.destroyAllWindows()