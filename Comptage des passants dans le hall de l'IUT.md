# Comptage des passants dans le hall de l'IUT

## Kubiak Victor

### Compte Rendu Projet

## Recherches scientifiques

Je me suis premièrement renseigné sur les différents moyens de détecter une personne sur un flux vidéo ou une image. Il existe énormément de techniques et d'algorithmes différents pour répondre à cette problématique.

Deux thématiques sont beaucoup ressorties pendant mes recherches :

- Modèle pour la classification
- Détection de mouvement

Je m'étais penché sur la question de la classification, mais la puissance de calcul demandée est trop importante. De plus sa mise en place est contraignante car un modèle doit être entrainé pour la reconnsaissance d'humain.

Compte tenu du fait que je n'ai pas accès au flux vidéo des caméras de l'IUT, j'ai préféré utilisé la détection de mouvement comme techniques pour le comptage des passant du hall. De plus je me suis fixé comme objectif d'essayer de rendre le processus de détection peu lourd quant à la charge de calcul requise.

---

## Choix des outils

Je me suis mis à la recherche de bibliothèques de traitement image/vidéo. Je connaissais déjà "scikit-image" que j'avais utilisé pour du traitement de signal sur des images.

J'ai aussi découvert Amazon Rekognition et le Google Cloud Vision API mais ces outils sont orientés sur le machine learning. De plus ils ne sont pas totalement gratuit.

J'ai également trouvé OpenCv (Open Computer Vision) qui est un bibliothèque graphique libre pour le traitement d'image en temps réel.

<img src="https://upload.wikimedia.org/wikipedia/commons/5/53/OpenCV_Logo_with_text.png" alt="logo openCV" width="100"/>

Elle est utilisable dans beaucoup de langages comme C++, Python, Java...
Et elle propose des outils intéressant pour la détection de mouvement et est un bibliothèque libre (licence BSD).

J'ai décidé d'utiliser le language python car je n'ai pas beaucoup utilisé ce langage comparé aux autres. Cela me permettra de me familiariser un peu plus avec celui-ci.

---

## Méthodologie initiale imaginée

Les résultats de ces recherches m'ont laissé pensé que le moyen suivant était la méthologie plus adapté pour mon cas

- Retirer le fond de l'image c'est-à-dire de retirer un pixel de l'image si celui-ci ne varie pas. cela me permettra de visualiser les formes en mouvements sur la vidéo.

- Analyser les formes en déplacements "Blob" (Binary Large Objects) afin de trier les faux positifs (bruit de l'image etc...).

- Échelonner et définir un zone de l'image pour détecter la position des blobs et enfin compter lorsque quelqu'un entre ou sort de la zone définie.

---

## Soustraction du fond de l'image

Pour mieux comprendre les différents algorithmes de soustraction de fond deux articles m'ont énormément aidé :

*Background Subtraction Algorithm Based Human Motion Detection  Rupali S.Rakibe, Bharati D.Patil*

Ainsi que celui-ci :

*M. B. Boubekeur, S. Luo and H. Labidi, "A background subtraction algorithm for indoor monitoring surveillance systems," 2015 IEEE International Conference on Computational Intelligence and Virtual Environments for Measurement Systems and Applications (CIVEMSA), Shenzhen, 2015, pp. 1-5, doi: 10.1109/CIVEMSA.2015.7158605.*

OpenCV propose déjà différents algorithmes de suppression de fond (MOG, MOG2, GMG, KNN). J'ai donc comparé les différents algorithmes et après quelques tests j'ai réussi à avoir des résultats convaincants avec le MOG2 qui permet la détection d'ombres (comparé au MOG).
A noter que le tests ont été effectués sur une webcam et que donc la qualité et l'exposition d'image ne sera pas la même en condition réelle.

<img src="https://i.imgur.com/mHD5fJW.png" width="400">

---

## Analyse de Blobs

Je suis actuellement en train de développer une solution pour l'analyse des Blobs. Le problème que je rencontre est que les flux vidéos que je possède comportent des bruits et des anomalies dus à des changement de luminosité ou à la qualité de la capture.

Rendu sur un flux vidéo de caméra de surveillance de la circulation routière par temps sombre :
<img src="https://imgur.com/7kfa3MW.png" width="500">

On peut voir que la qualité de la soustraction de fond est amoindrie par la pauvre luminosité. De ce fait, un seul blob est détecté à droite de l'image (cercle rouge).

Pour obtenir de meilleurs résultats il faut modifier les propriétés des filtres pour les rendre plus ou moins sensibles en fonction de la qualité d'image. Je modifie donc les différents paramètres afin de comprendre comment se comportent les algorithmes en fonction des qualités des vidéos.

---
## Gantt avancement du projet

<img src="https://i.imgur.com/iUyQq6P.png">

---

## Sources

Article scientifique : "Background Subtraction Algorithm Based Human Motion Detection"
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.414.5782&rep=rep1&type=pdf

Différents algorithmes de soustraction de fond d'image 
https://stackoverflow.com/questions/33266239/differences-between-mog-mog2-and-gmg

Vidéo de comparaison des algorithmes de soustraction de fond
https://www.youtube.com/watch?v=M6yUlAhxBxk

Détection de Blobs
https://www.learnopencv.com/blob-detection-using-opencv-python-c/

OpenCV
https://opencv.org/

