import numpy as np
import cv2 as cv

import sys
np.set_printoptions(threshold=sys.maxsize)

img = cv.imread('/home/enzo/Victor/ido-security-cam/images/lion.jpeg')
gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
gray = np.float32(gray)
dst = cv.cornerHarris(gray,2,3,0.001)
#result is dilated for marking the corners, not important
#dst = cv.dilate(dst,None)

#mask = np.zeros_like(gray)
#mask[dst>0.01*dst.max()] = 255
# Threshold for an optimal value, it may vary depending on the image.
img[dst>0.01*dst.max()]=[0,0,255]
tuplesCoordinates = np.where(np.all(img == (0,0,255), axis=-1))

# invert axis Y, X (inverted) and combine 2 Tuple coordinate into list of coordinates.
coordinates = list(zip(tuplesCoordinates[1],tuplesCoordinates[0]))

cv.namedWindow('image', cv.WINDOW_NORMAL)
cv.resizeWindow('image', 1280,720)
while(1):
    cv.imshow('image',img)
    k = cv.waitKey(33)
    if k==27:    # Esc key to stop
        break
    elif k==-1:  # normally -1 returned,so don't print it
        continue