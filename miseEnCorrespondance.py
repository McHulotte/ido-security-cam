import math
import numpy as np
import cv2 as cv
import copy
import time
MASK=20

def storeClick(event,x,y,flags,param):
    global mouseX,mouseY
    if event == cv.EVENT_LBUTTONDBLCLK:
        mouseX,mouseY = x,y

def tableauPixels():
    # Parcours l'image
    for i in range(imgL.shape[1]):
        for j in range(imgL.shape[0]):
            bgr = cv.split(imgL[j,i])         
            # Les pixels rouge sont stockés
            if (bgr[0][0] == 0 and bgr[0][1] == 0 and bgr[0][2] == 255):
                coordinates.append([j,i])
    return coordinates

def computeFullScoreFromCoordinates(v):
    ##### Acces aux voisinage de l'autre image a partir des pixels de contour
    score = 0
    for i in range(len(coordinates)):
        x = coordinates[i][0]
        y = coordinates[i][1]
        sum = 0
        for j in range(x-v,x+v+1):
            for k in range(y-v,y+v+1):
                # separation des canneaux
                bgrL = cv.split(img[j,k])
                bgrR = cv.split(imgR[j,k])
                # calcul de la luminescence
                lumiL = (bgrL[0][0] + bgrL[0][1] + bgrL[0][2])/3
                lumiR = (bgrR[0][0] + bgrR[0][1] + bgrR[0][2])/3

                sum += (lumiL-lumiR)**2
            score += sum
    return score

def computeScoreFromClick(v,mask):
    
    lumiL = np.empty([v*2+1,v*2+1], dtype=int)
    lumiR = np.empty([v*2+1,v*2+1], dtype=int)

    # calcul de la luminescence du pixel pointé a gauche
    countx=0
    county=0
    bL, gL, rL = cv.split(imgL)
    for i in range(mouseY-v, mouseY+v+1):
        for j in range(mouseX-v, mouseX+v+1):
            lumiL[countx,county] = (int(bL[i,j]) + int(gL[i,j]) + int(rL[i,j]))/3
            countx += 1
        county += 1
        countx = 0
    county=0
   
    score = 99999999  
    tempScore = 0
    xCoord = 0
    yCoord = 0

    # Parcours du masque
    for i in range(mouseY-mask, mouseY+mask+1):
        for j in range(mouseX-mask, mouseX+mask+1):
            # Parcours du voisinage dans chaque pixel du masque
            for k in range(i-v, i+v+1):
                for l in range(j-v, j+v+1):
                    #print(k,l)
                    bgrR = cv.split(imgR[k,l])
                    lumiR[countx,county] = (int(bgrR[0][0]) + int(bgrR[0][1]) + int(bgrR[0][2]))/3
                    countx += 1
                county += 1
                countx = 0
            county=0


            # carre des termes de la somme
            for m in range(v*2+1):
                for n in range(v*2+1):
                    tempScore += (lumiL[m,n]-lumiR[m,n])**2
           
            if (tempScore < score):
                score = tempScore
                xCoord = j
                yCoord = i

            tempScore = 0
    return score, xCoord, yCoord, lumiL

def drawCross(x,y, mask):
    cv.line(imgR,(x-5,y),(x+5,y),(0,0,255),1)
    cv.line(imgR,(x,y-5),(x,y+5),(0,0,255),1)

    for i in range(mouseX-mask, mouseX+mask+1):
        imgR[mouseY-mask,i]=[255,0,0]
        imgR[mouseY+mask,i]=[255,0,0]
    for j in range(mouseY-mask, mouseY+mask+1):
        imgR[j,mouseX-mask]=[255,0,0]
        imgR[j,mouseX+mask]=[255,0,0]

def savePicture(pic):
    file = open("/home/enzo/Victor/ido-security-cam/images/img.pgm","w+")
    file.writelines("P2\n#Test du commentaire dans fichier\n")
    file.write(str(pic.shape[0]) + " " + str(pic.shape[1]) + "\n")
    file.write("15\n")
    for i in range(pic.shape[0]):
        for j in range(pic.shape[1]):
            pic[i,j] = ((pic[i,j])/255.0)*15.0
            file.write(str(pic[i,j]) + " ")
        file.write("\n")
    file.close()

imgL = cv.imread("/home/enzo/Victor/ido-security-cam/images/pictureL.jpg")
imgR = cv.imread("/home/enzo/Victor/ido-security-cam/images/pictureR.jpg")
img = copy.deepcopy(imgL)

coordinates = []
grayL = cv.cvtColor(imgL,cv.COLOR_BGR2GRAY)
grayL = np.float32(grayL)
dst = cv.cornerHarris(grayL,2,3,0.05)

#result is dilated for marking the corners, not important
dst = cv.dilate(dst,None)
mask = np.zeros_like(grayL)
mask[dst>0.01*dst.max()] = 255
# Threshold for an optimal value, it may vary depending on the image.

# corners en rouge
imgL[dst>0.01*dst.max()]=[0, 0, 255]
#coordinates = tableauPixels()
#print(computeFullScoreFromCoordinates(1))


cv.namedWindow('image')
cv.setMouseCallback('image',storeClick)
while(1):
    cv.imshow('image',img)
    k = cv.waitKey(20) & 0xFF
    if k == 27:
        print(mouseX,mouseY)
        break

start_time = time.time()
result = computeScoreFromClick(10,MASK)
print("--- %s seconds ---" % (time.time() - start_time))
savePicture(result[3])

drawCross(result[1],result[2],MASK)
cv.imshow('imageR',imgR)
if cv.waitKey(0) & 0xff == 27:
    cv.destroyAllWindows()



