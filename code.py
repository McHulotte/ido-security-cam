import cv2
import pandas
import numpy as np
from datetime import datetime
# video height/height
height = 900
width = 600

# initialize video capture
#cam = cv2.VideoCapture(0)
#http://195.209.186.44:80/mjpg/video.mjpg
cam = cv2.VideoCapture(0)
# List pour les mouvements
liste_mouvemnt = [None, None]
# temps du mouvement
time = []
dataFrame = pandas.DataFrame(columns = ["start","end"])
fond = cv2.createBackgroundSubtractorMOG2(400,100,None)
frame_cpt = 0

#cv2.createBackgroundSubtractor
while True:
    # Capture image par image
    ret, frame = cam.read()

    if not ret:
        break

    frame_cpt += 1
    
    mask = fond.apply(frame)
    mogCount = np.count_nonzero(mask)
    #print('Mog2 frame : %d, Pixel count: %d' % (frame_cpt, mogCount))
    
    # Définition des paramètres de la détection de blob
    """params = cv2.SimpleBlobDetector_Params()
    params.blobColor = 255
    params.filterByColor = True
    params.filterByArea = True
    params.minArea = 70 """
    # création du détecteur de blob
    blobDetector = cv2.SimpleBlobDetector_create()
    keyPoints = blobDetector.detect(mask)
    mask_with_keypoints = cv2.drawKeypoints(mask, keyPoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    print(len(keyPoints))
    
    ##################################################
    
    # Je trace une ligne
    #frame = cv2.line(frame,(600,720),(600,530),(255,0,0),2)
    
    # Passer en Grayscale
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cv2.imshow("video",frame)
    cv2.imshow("mask", mask_with_keypoints)
    if cv2.waitKey(1) == 27:
        break

    

# When everything done, release the capture
cam.release()
cv2.destroyAllWindows()