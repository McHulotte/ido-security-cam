from typing import NamedTuple

import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient

INFLUXDB_ADDRESS = '172.20.1.3'
INFLUXDB_USER = 'victor'
INFLUXDB_PASSWORD = 'victor'
INFLUXDB_DATABASE = 'passant'

MQTT_ADDRESS = '172.20.1.7'
MQTT_USER = 'cvictor'
MQTT_PASSWORD = 'cvictor'
MQTT_TOPIC = 'home'
MQTT_CLIENT_ID = 'MQTTInfluxDBBridge'

influxdb_client = InfluxDBClient(INFLUXDB_ADDRESS, 8086, INFLUXDB_USER, INFLUXDB_PASSWORD, None)

class Camera(NamedTuple):
    gens : str
    nbGens: int 

def on_connect(client, userdata, flags, rc):
    """ The callback for when the client receives a CONNACK response from the server."""
    print('Connected with result code ' + str(rc))
    client.subscribe(MQTT_TOPIC)

def _parse_mqtt_message(topic, payload):
    #match = re.match(MQTT_REGEX, topic)

    measurement = topic
    return Camera(measurement,int(payload))

def _send_sensor_data_to_influxdb(camera):
    json_body = [
        {
            'measurement': camera.gens,
            'fields': {
                'nb_gens': camera.nbGens
            }
        }
    ]
    influxdb_client.write_points(json_body)

def on_message(client, userdata, msg):
    """The callback for when a PUBLISH message is received from the server."""
    print(msg.topic + ' ' + str(msg.payload))
    cam = _parse_mqtt_message(msg.topic, msg.payload.decode('utf-8'))
    if cam is not None:
        _send_sensor_data_to_influxdb(cam)

def _init_influxdb_database():
    databases = influxdb_client.get_list_database()
    if len(list(filter(lambda x: x['name'] == INFLUXDB_DATABASE, databases))) == 0:
        influxdb_client.create_database(INFLUXDB_DATABASE)
    influxdb_client.switch_database(INFLUXDB_DATABASE)

def main():
    _init_influxdb_database()

    mqtt_client = mqtt.Client(MQTT_CLIENT_ID)
    mqtt_client.username_pw_set(MQTT_USER, MQTT_PASSWORD)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message

    mqtt_client.connect(MQTT_ADDRESS, 1883)
    mqtt_client.loop_forever()


if __name__ == '__main__':
    print('MQTT to InfluxDB bridge')
    main()