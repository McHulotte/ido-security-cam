import cv2
import os
rtsp = "rtsp:172.42.100.100:554/media/1/1"
rtsp2 = "rtsp:172.42.100.101:554/media/1/1"

camL = cv2.VideoCapture(rtsp)
camR = cv2.VideoCapture(rtsp2)

cv2.namedWindow("test")
path = "/home/enzo/Victor/ido-security-cam/savedPicsV3"

img_counter = 0

while True:
    ret, frame = camL.read()
    ret2, frame2 = camR.read()
    if not ret or not ret2:
        print("can't read camera")
        break
    cv2.imshow("CameraL", frame)
    cv2.imshow("CameraR", frame2)

    k = cv2.waitKey(1)
    if k%256 == 27:
        # ESC pressed
        print("Closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        img_name = "picL_{}.png".format(img_counter)
        img_name2 = "picR_{}.png".format(img_counter)
        cv2.imwrite(os.path.join(path, img_name), frame)
        cv2.imwrite(os.path.join(path, img_name2), frame2)
        print("{} sauvegardé !".format(img_name))       
        print("{} sauvegardé !".format(img_name2))
        img_counter += 1

camL.release()
camR.release()

cv2.destroyAllWindows()